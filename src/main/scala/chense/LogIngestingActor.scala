package chense

import akka.actor.{ Actor, Props }
import akka.actor.ActorSystem

case object StartFlag

case class LogMessage(message: String)

class LogIngestingActor extends Actor {

  // Create the 'helloakka' actor system'
  val system = ActorSystem("helloakka")

  // Create the 'greeter' actor
  val lpa = system.actorOf(Props[LogProcessingActor], "LogProcessingActor")

  private val lcr = LogChunkReader("http://10.8.27.34/log4j/catalina.2013-06-18.log")

  var skipSize = 1

  def receive() = {
    case StartFlag => {
      lpa.!(LogMessage(new String(lcr.bufferize(skipSize = skipSize))))
    }

    case "Incre" => {
      if (skipSize < 10) {
        skipSize = skipSize + 1
        println(skipSize)
        sender.!(LogMessage(new String(lcr.bufferize(skipSize = skipSize))))
      }
    }
  }

}

class LogProcessingActor extends Actor {

  def receive() = {
    case LogMessage(msg) => {
      println("received msg " + msg)
      sender.!("Incre")
    }
  }
}

object LogProcessing extends App {

  // Create the 'helloakka' actor system'
  val system = ActorSystem("helloakka")

  // Create the 'greeter' actor
  val lpa = system.actorOf(Props[LogIngestingActor], "LogIngestingActor")

  lpa.!(StartFlag)

}