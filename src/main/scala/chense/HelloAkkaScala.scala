package chense

import akka.actor.{ ActorRef, ActorSystem, Props, Actor, Inbox }
import scala.concurrent.duration._
import akka.actor.actorRef2Scala

case object Greet
case class WhoToGreet(who: String)

class Greeter extends Actor {
  var greeting = ""

  def receive = {
    case WhoToGreet(who) => {
      greeting = s"hello, $who"
      println(greeting)
    }
  }
}

object HelloAkkaScala extends App {

  // Create the 'helloakka' actor system
  val system = ActorSystem("helloakka")

  // Create the 'greeter' actor
  val greeter = system.actorOf(Props[Greeter], "greeter")

  greeter.!(WhoToGreet("first message"))

  system.shutdown()
}
