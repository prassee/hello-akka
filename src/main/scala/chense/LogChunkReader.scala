package chense

import java.io.BufferedInputStream
import java.net.URL

object LogChunkReader {
  def apply(url: String) = new LogChunkReader(url)
}
 
class LogChunkReader(url: String) {
 
  def bufferize(blocksize: Int = 1024, skipSize: Int) = {
    val is = new URL(url).openStream()
    val buff = new Array[Byte](blocksize)
    val bis = new BufferedInputStream(is, 8 * blocksize)
    if (skipSize == 1) {
      bis.skip(0)
    } else {
      bis.mark(skipSize * blocksize)
      bis.skip(skipSize * blocksize)
    }
    bis.read(buff, 0, blocksize)
    buff
  }
}

object Main extends App {
  val lcr = LogChunkReader("http://10.8.27.34/log4j/catalina.2013-06-18.log")
  println(new String(lcr.bufferize(skipSize = 1)))
}